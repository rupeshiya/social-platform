const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { sendEmail } = require('../helpers/index');


exports.registerUser = (req, res) => {
  // take name email username  password and save 
  // check if user already exists 
  User.findOne({email: req.body.email })
    .then((user)=>{
      if(user){
        return res.status(400).json({success: false, msg: 'Email is already registered!'});
      } else {
        // create a new user and save 
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
          username: req.body.username
        });
        // hash the password
        bcrypt.genSalt(10, (err, salt)=>{
          if(err){
            console.log(err);
          }
          bcrypt.hash(newUser.password, salt ,(err, hash)=>{
            newUser.password = hash;
            newUser.save()
            .then((user)=>{
              console.log("Successfully registered!");
              res.status(201).json({success: true, msg: 'Successfully registered!'});
            })
            .catch((err)=>{
              console.log("Error in hashing the password!", err);
            });
          });
        });
      }
    });
}

// locally login 
exports.loginUser = (req, res) => {
  User.findOne({email: req.body.email})
    .then((user)=>{
      if(!user){
        return res.status(400).json({success: false, msg: 'User does not exists!!'});
      } else {
        // if user exists decode and compare the password 
        bcrypt.compare(req.body.password, user.password, (err, success)=>{
          if(err){
            console.log(err);
          } 
          if(success){
            // res.json({success: true, msg: 'successfully loggedIn'});
            const payload = { id: user._id, email: user.email };
            // create token
            jwt.sign(payload, process.env.secret, {expiresIn: 3600},(err, token)=>{
              res.json({success: true, token: 'Bearer ' + token });
            })
          }
        })
      }
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err: err });
    });
}
 
// forgot password (send link to the email for changing password) 
exports.forgotPassword = (req, res) => {
  const email = req.body.email;
  User.findById(req.user._id)
    .then((user)=>{
      if(!user){
        return res.status(401).json({success: false, msg: 'Unauthorized!'});
      }
      // generate token for changing the password 
      const token = jwt.sign({_id: req.user._id, pass: req.user.password}, process.env.secret)
      const emailData = {
        from: 'noreply@node-react.com',
        to: email,
        subject: 'Password Reset Instructions',
        text: `Please use the following link to reset your password: ${
                process.env.CLIENT_URL
            }/reset-password/${token}`,
        html: `<p>Please use the following link to reset your password:</p> <p>${
                process.env.CLIENT_URL
            }/reset-password/${token}</p>`
      };

      // update user db by new generated resetPasswordToken 
      user.resetPasswordToken = token;
      user.save()
        .then((result)=>{
          console.log("successfully saved the resetPasswordToken", result);
          sendEmail(emailData);
          return res.status(201).json({success: true, msg: `Please check your registered ${email} for reset password link!`});
        });
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err});
    });
}

// reset password
exports.resetPassword = (req, res) => {
  // get token and newPassword 
  const { resetPasswordToken, newPassword } = req.body;
  User.findOne({ resetPasswordToken: resetPasswordToken })
    .then((user)=>{
      if(!user){
        return res.status(401).json({success: false, msg: 'Invalid link!'});
      }
      // update the user db token value to undefined 
      user.resetPasswordToken = undefined;
      bcrypt.genSalt(10, (err, salt)=>{
        bcrypt.hash(newPassword, salt , (err, hash)=>{
          user.password = hash;
          user.save()
          .then((result)=>{
            console.log("successfully updated the password!", result);
            return res.status(200).json({success: true, msg: 'Successfully updated the password!'});
          });
        });
      });
    })
    .catch((err)=>{
      console.log(err);
      return res.status(400).json({success: false, err });
    });
}