const express = require('express');
const router = express.Router();
const { 
  createPost,
  getPosts,
  getAllPostOfAUser,
  deletePostById,
  addComment,
  deleteComment,
  likePost,
  unlikePost
 } = require('../controllers/post.controller');

const passport = require('passport');
const validators = require('../validators/postValidator');

// get all posts 
router.get('/post/all', passport.authenticate('jwt', { session: false }), getPosts);

// create a post 
router.post('/post/create', validators.createPostValidator, passport.authenticate('jwt', { session: false }), createPost);

// get post all the post of a user 
router.get('/post/all/:userId', passport.authenticate('jwt', { session: false }), getAllPostOfAUser);

// delete post 
router.delete('/post/delete/:postId', passport.authenticate('jwt', { session: false }), deletePostById);

// like post
router.post('/post/like/:postId', passport.authenticate('jwt', { session: false}), likePost);

// unlike post 
router.post('/post/unlike/:postId', passport.authenticate('jwt', { session: false}), unlikePost);

// add comment 
router.post('/post/comment/:postId', passport.authenticate('jwt', { session: false }), addComment);

// delete comment 
router.delete('/post/comment/delete/:postId', passport.authenticate('jwt', { session: false }), deleteComment);


module.exports = router;