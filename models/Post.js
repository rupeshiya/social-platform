const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
  title:{
    type: String,
    required: "Title is required",
  },
  body:{
    type: String,
    required: "Body of the post is required",
  },
  photo:{
    data: Buffer,
    contentType: String
  },
  postedBy:{
    type:mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  created:{
    type: Date,
    default: Date.now()
  },
  likes:[
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  ],
  comments: [
    {
      text: {
        type: String
      },
      created: {
        type: Date,
        default: Date.now()
      },
      postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    }
  ]
});

module.exports = mongoose.model('Post', postSchema);