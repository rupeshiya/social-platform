const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: "Name is required"
  },
  username:{
    type: String,
    required: "Username is required"
  },
  email:{
    type: String,
    required: "Email is required"
  },
  password:{
    type:String,
    required: "Password is required"
  },
  created:{
    type: Date,
    default: Date.now()
  },
  updated: {
    type: Date
  },
  followings:[
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  ],
  followers: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  ],
  resetPasswordToken: {
    type: String
  },
  role:{
    type: String
  }
});

module.exports = mongoose.model('User', userSchema);