exports.createPostValidator = (req, res, next) => {
  // validation for title
  req.check('title', 'Title is required!').notEmpty();
  req.check('title', 'Must be of length between 4 and 150', isLength({
    min: 4,
    max: 150
  }));
  //validation for the body 
  req.check('body', 'Body is required!').notEmpty();
  req.check('body', 'Body should be of length', isLength({
    min: 4,
    max: 2000,
  }))
  // get errors 
  const errors = req.validationErrors();
  if(errors){
    // get the first error
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({success: false, error: firstError });
  }
  next();
}